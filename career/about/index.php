<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Новости");
$APPLICATION->AddHeadScript('/js/main-page-slider.js');
$APPLICATION->AddHeadScript('/js/story-slider.js');

?>
<section class="career-header">
  <div class="career-header__wrap container-fluid">
    <div class="row">
      <div class="col-md-4 col-sm-8 col-xs-8">
        <a href="/career/" class="career-header__logo">Карьера</a>
      </div>
      <div class="col-md-22 col-sm-30 col-xs-30 mobile-nav">
        <nav class="career-header__nav career-nav">
          <ul class="career-nav__list">
            <li class="career-nav__item"><a class="career-nav__link" href="#">Профессионалам</a></li>
            <li class="career-nav__item"><a class="career-nav__link" href="#">Студентам и выпускникам</a></li>
            <li class="career-nav__item"><a class="career-nav__link" href="#">О работе в компании</a></li>
            <li class="career-nav__item"><a class="career-nav__link" href="#">Вакансии</a></li>
          </ul>
        </nav>
      </div>
      <div class="col-md-3 col-md-offset-1 col-md-3 col-md-offset-1 col-sm-6 col-sm-offset-16 col-xs-22">
        <input class="career-header__button career-button career-button_login" type="submit" name="login" value="Вход">
      </div>
    </div>
  </div>
</section>
<section class="career-breadcrumbs">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-30 col-sm-30 col-xs-30">
        <ul class="career-breadcrumbs__list">
          <a class="career-breadcrumbs__item">О процессе найма</a>
          <li class="career-breadcrumbs__item career-breadcrumbs__item_active">Новости и интервью</li>
        </ul>
      </div>
    </div>
  </div>
</section>
<section class="career-about">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-30 col-sm-30 col-xs-30">
        <h1 class="career-h1 career-about__title">О процессе найма</h1>
        <div class="row">
          <div class="col-md-25 col-sm-25 col-xs-30">
            <h2 class="career-h2 career-about__subtitle">Чтобы стать кандидатом на заинтересовавшую вас вакансию, необходимо подать онлайн заявку. Для этого нужно:</h2>
          </div>
        </div>
        <ul class="about-steps career-about__list">
          <li class="about-steps__item">
            <strong class="about-steps__title"><a class="about-steps__link" href="#">Зарегистрироваться</a> и создать личный кабинет соискателя</strong>
          </li>
          <li class="about-steps__item">
            <strong class="about-steps__title">В личном кабинете вы можете направить свое резюме (откликнуться на вакансию).</strong>
            <p class="about-steps__text">
              Если вы не нашли интересующую вакансию, вы можете направить свое резюме в нашу базу данных. Ваши данные станут также доступными для просмотра нашими специалистами по подбору персонала, которые, может быть, свяжутся с вами, когда откроется подходящая вакансия.
            </p>
          </li>
          <li class="about-steps__item">
            <strong class="about-steps__title">С вами свяжется специалист по подбору персонала и проведет телефонное интервью</strong>
            <p class="about-steps__text">Если ваше резюме окажется подходящим для выбранной вакансии.</p>
          </li>
          <li class="about-steps__item">
            <strong class="about-steps__title">
              Финальное решение о найме принимается на основе сравнения результатов оценки всех кандидатов, участвовавших в конкурсе
            </strong>
          </li>
          <li class="about-steps__item">
            <strong class="about-steps__title">Если вы успешно прошли интервью, мы пригласим вас на следующие этапы:</strong>
            <p class="about-steps__text">
              Тестирование и  очное интервью с руководителем (и другими участниками процедур оценки кандидатов). Мы приложим все усилия, чтобы известить вас об итогах оценки в течение нескольких дней.
            </p>
          </li>
        </ul>
        <a href="#" class="career-button about-steps__button">Зарегистрироваться</a>
      </div>
    </div>
  </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
