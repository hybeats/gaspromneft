<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Газпром Нефть");

$APPLICATION->AddHeadScript('/js/main-page-slider.js');
$APPLICATION->AddHeadScript('/js/stats-slider.js');
$APPLICATION->AddHeadScript('/js/main-page-video.js');
?>
<section class="career-header">
  <div class="career-header__wrap container-fluid">
    <div class="row">
      <div class="col-md-4 col-sm-8 col-xs-8">
        <a href="/career/" class="career-header__logo">Карьера</a>
      </div>
      <div class="col-md-22 col-sm-30 col-xs-30 mobile-nav">
        <nav class="career-header__nav career-nav">
          <ul class="career-nav__list">
            <li class="career-nav__item"><a class="career-nav__link" href="#">Профессионалам</a></li>
            <li class="career-nav__item"><a class="career-nav__link" href="#">Студентам и выпускникам</a></li>
            <li class="career-nav__item"><a class="career-nav__link" href="#">О работе в компании</a></li>
            <li class="career-nav__item"><a class="career-nav__link" href="#">Вакансии</a></li>
          </ul>
        </nav>
      </div>
      <div class="col-md-3 col-md-offset-1 col-md-3 col-md-offset-1 col-sm-6 col-sm-offset-16 col-xs-22">
        <input class="career-header__button career-button career-button_login" type="submit" name="login" value="Вход">
      </div>
    </div>
  </div>
</section>
<section class="career-banner">
  <div class="container-fluid career-banner__title">
    <div class="row">
      <div class="col-md-15 col-md-offset-15 col-sm-15 col-sm-offset-15 col-xs-18 col-xs-offset-12">
        <h1 class="career-h1">Создавай историю вместе с нами </h1>
      </div>
    </div>
  </div>
  <div class="container-fluid career-banner__links banner-links">
    <div class="row">
      <div class="col-md-14 col-sm-14 col-xs-30">
        <a href="" class="banner-links__item">Профессионалам</a>
      </div>
      <div class="col-md-14 col-md-offset-2 col-sm-14 col-sm-offset-2 col-xs-30">
        <a href="" class="banner-links__item">Студентам и выпускникам</a>
      </div>
    </div>
  </div>
</section>
<section class="career-promo">
  <div class="container-fluid career-promo__text">
    <div class="row">
      <div class="col-md-30">
        <h2 class="career-promo__title">«Газпром нефть» – лучший работодатель</h2>
        <h3 class="career-promo__subtitle">по версии HeadHunter и Universum</h3>
      </div>
    </div>
  </div>
</section>
<section class="career-video">
  <div class="container-fluid career-video__text">
    <div class="row">
      <div class="col-md-20 col-sm-25 col-xs-30">
        <h2 class="career-video__title">Работа в «Газпром нефти» – это участие в масштабных проектах и решение интересных профессиональных задач</h2>
      </div>
    </div>
  </div>
  <span class="career-video__play"></span>
  <div class="career-video__wrap">
    <video id="main-video" poster="/images/video-poster.jpg" tabindex="0"> <!--  -->
      <source src="" type='video/webm; codecs="vp8, vorbis"' />
      <source src="" type='video/ogg; codecs="theora, vorbis"' />
    </video>
  </div>
</section>
<section class="career-people">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-30">
        <div class="swiper-container stats-slider">
          <div class="swiper-wrapper">
            <div class="swiper-slide stats-slider__slide">
              <span class="stats-slider__number stats-slider__number_more">50 000</span>
              <p class="stats-slider__text">сотрудников компании ежегодно проходят обучение</p>
            </div>
            <div class="swiper-slide stats-slider__slide">
              <span class="stats-slider__number">24%</span>
              <p class="stats-slider__text">сотрудников моложе 30 лет</p>
            </div>
            <div class="swiper-slide stats-slider__slide">
              <span class="stats-slider__number stats-slider__number_more">65 000</span>
              <p class="stats-slider__text">человек работают в группе «Газпром нефть»</p>
            </div>
          </div>
          <div class="swiper-pagination"></div>
        </div>
      </div>
    </div>
  </div>

  <div class="career-people__slider people-slider">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-30 content-wrap content-wrap_main_slider">
          <div class="swiper-container people-slider__main">
            <div class="swiper-wrapper">
              <div class="swiper-slide" style="background-image:url(/images/people-slider/1.jpg); background-size: cover;  background-position: center;">
                <div class="row">
                  <div class="col-md-18 col-sm-20 col-xs-20">
                    <span class="people-slider__rubric">Люди дела</span>
                    <span class="people-slider__title">«Наша философия в том, что всегда надо быть на полшага впереди наших партнёров, коллег, конкурентов».</span>
                    <span class="people-slider__author">Генеральный директор «Газпромнефть-Аэро»<br/>Владимир Егоров</span>
                  </div>
                </div>
              </div>
              <div class="swiper-slide" style="background-image:url(/images/people-slider/1.jpg); background-size: cover;  background-position: center;">
                <div class="row">
                  <div class="col-md-18 col-sm-20 col-xs-20">
                    <span class="people-slider__rubric">Люди дела</span>
                    <span class="people-slider__title">«Наша философия в том, что всегда надо быть на полшага впереди наших партнёров, коллег, конкурентов».</span>
                    <span class="people-slider__author">Генеральный директор «Газпромнефть-Аэро»<br/>Владимир Егоров</span>
                  </div>
                </div>
              </div>
              <div class="swiper-slide" style="background-image:url(/images/people-slider/1.jpg); background-size: cover;  background-position: center;">
                <div class="row">
                  <div class="col-md-18 col-sm-20 col-xs-20">
                    <span class="people-slider__rubric">Люди дела</span>
                    <span class="people-slider__title">«Наша философия в том, что всегда надо быть на полшага впереди наших партнёров, коллег, конкурентов».</span>
                    <span class="people-slider__author">Генеральный директор «Газпромнефть-Аэро»<br/>Владимир Егоров</span>
                  </div>
                </div>
              </div>
              <div class="swiper-slide" style="background-image:url(/images/people-slider/1.jpg); background-size: cover;  background-position: center;">
                <div class="row">
                  <div class="col-md-18 col-sm-20 col-xs-20">
                    <span class="people-slider__rubric">Люди дела</span>
                    <span class="people-slider__title">«Наша философия в том, что всегда надо быть на полшага впереди наших партнёров, коллег, конкурентов».</span>
                    <span class="people-slider__author">Генеральный директор «Газпромнефть-Аэро»<br/>Владимир Егоров</span>
                  </div>
                </div>
              </div>
              <div class="swiper-slide" style="background-image:url(/images/people-slider/1.jpg); background-size: cover; background-position: center;">
                <div class="row">
                  <div class="col-md-18 col-sm-20 col-xs-20">
                    <span class="people-slider__rubric">Люди дела</span>
                    <span class="people-slider__title">«Наша философия в том, что всегда надо быть на полшага впереди наших партнёров, коллег, конкурентов».</span>
                    <span class="people-slider__author">Генеральный директор «Газпромнефть-Аэро»<br/>Владимир Егоров</span>
                  </div>
                </div>
              </div>
              <div class="swiper-slide" style="background-image:url(/images/people-slider/1.jpg); background-size: cover;  background-position: center;">
                <div class="row">
                  <div class="col-md-18 col-sm-20 col-xs-20">
                    <span class="people-slider__rubric">Люди дела</span>
                    <span class="people-slider__title">«Наша философия в том, что всегда надо быть на полшага впереди наших партнёров, коллег, конкурентов».</span>
                    <span class="people-slider__author">Генеральный директор «Газпромнефть-Аэро»<br/>Владимир Егоров</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="people-slider__thumbs people-thumbs">
            <div class="swiper-container people-thumbs__wrap">
              <div class="swiper-wrapper">
                <div class="swiper-slide" style="background-image:url(/images/people-slider/1.jpg); background-size: cover;"></div>
                <div class="swiper-slide" style="background-image:url(/images/people-slider/1.jpg); background-size: cover;"></div>
                <div class="swiper-slide" style="background-image:url(/images/people-slider/1.jpg); background-size: cover;"></div>
                <div class="swiper-slide" style="background-image:url(/images/people-slider/1.jpg); background-size: cover;"></div>
                <div class="swiper-slide" style="background-image:url(/images/people-slider/1.jpg); background-size: cover;"></div>
                <div class="swiper-slide" style="background-image:url(/images/people-slider/1.jpg); background-size: cover;"></div>
              </div>
            </div>
            <div class="people-thumbs__next"></div>
            <div class="people-thumbs__prev"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="career-wrap career-wrap_main_news">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-30 col-sm-30 col-xs-30">
        <div class="news">
          <h2 class="news__title">Новости</h2>
          <ul class="news__list">
            <li class="news__item">
              <a href="#" class="news__name">«Газпром нефть» стала лучшим работодателем России</a>
              <span class="news__date">18 февраля 2016</span>
            </li>
            <li class="news__item">
              <a href="#" class="news__name">«Газпром нефть» заключила соглашение о сотрудничестве с Тюменским государственным университетом</a>
              <span class="news__date">18 февраля 2016</span>
            </li>
            <li class="news__item">
              <a href="#" class="news__name">«Газпром нефть» стала лучшим работодателем России</a>
              <span class="news__date">18 февраля 2016</span>
            </li>
          </ul>
          <a href="#" class="news__more">Все новости</a>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="career-socmedia">
  <ul class="career-socmedia__list">
    <li class="career-socmedia__item career-socmedia__item_vk">47</li>
    <li class="career-socmedia__item career-socmedia__item_fb">22</li>
    <li class="career-socmedia__item career-socmedia__item_tw">13</li>
  </ul>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
