<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Новости");
$APPLICATION->AddHeadScript('/js/main-page-slider.js');
$APPLICATION->AddHeadScript('/js/story-slider.js');

?>
<section class="career-header">
  <div class="career-header__wrap container-fluid">
    <div class="row">
      <div class="col-md-4 col-sm-8 col-xs-8">
        <a href="/career/" class="career-header__logo">Карьера</a>
      </div>
      <div class="col-md-22 col-sm-30 col-xs-30 mobile-nav">
        <nav class="career-header__nav career-nav">
          <ul class="career-nav__list">
            <li class="career-nav__item"><a class="career-nav__link" href="#">Профессионалам</a></li>
            <li class="career-nav__item"><a class="career-nav__link" href="#">Студентам и выпускникам</a></li>
            <li class="career-nav__item"><a class="career-nav__link" href="#">О работе в компании</a></li>
            <li class="career-nav__item"><a class="career-nav__link" href="#">Вакансии</a></li>
          </ul>
        </nav>
      </div>
      <div class="col-md-3 col-md-offset-1 col-md-3 col-md-offset-1 col-sm-6 col-sm-offset-16 col-xs-22">
        <input class="career-header__button career-button career-button_login" type="submit" name="login" value="Вход">
      </div>
    </div>
  </div>
</section>
<section class="career-breadcrumbs">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-30 col-sm-30 col-xs-30">
        <ul class="career-breadcrumbs__list">
          <a class="career-breadcrumbs__item">О процессе найма</a>
          <li class="career-breadcrumbs__item career-breadcrumbs__item_active">Новости и интервью</li>
        </ul>
      </div>
    </div>
  </div>
</section>
<section class="career-wrap career-wrap_news">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-30 content-wrap content-wrap_news_slider">
        <div class="swiper-container people-slider__main">
          <div class="swiper-wrapper">
            <div class="swiper-slide" style="background-image:url(/images/people-slider/1.jpg); background-size: cover;  background-position: center;">
              <div class="row">
                <div class="col-md-18 col-sm-20 col-xs-20">
                  <span class="people-slider__rubric">Люди дела</span>
                  <span class="people-slider__title">«Наша философия в том, что всегда надо быть на полшага впереди наших партнёров, коллег, конкурентов».</span>
                  <span class="people-slider__author">Генеральный директор «Газпромнефть-Аэро»<br/>Владимир Егоров</span>
                </div>
              </div>
            </div>
            <div class="swiper-slide" style="background-image:url(/images/people-slider/1.jpg); background-size: cover;  background-position: center;">
              <div class="row">
                <div class="col-md-18 col-sm-20 col-xs-20">
                  <span class="people-slider__rubric">Люди дела</span>
                  <span class="people-slider__title">«Наша философия в том, что всегда надо быть на полшага впереди наших партнёров, коллег, конкурентов».</span>
                  <span class="people-slider__author">Генеральный директор «Газпромнефть-Аэро»<br/>Владимир Егоров</span>
                </div>
              </div>
            </div>
            <div class="swiper-slide" style="background-image:url(/images/people-slider/1.jpg); background-size: cover;  background-position: center;">
              <div class="row">
                <div class="col-md-18 col-sm-20 col-xs-20">
                  <span class="people-slider__rubric">Люди дела</span>
                  <span class="people-slider__title">«Наша философия в том, что всегда надо быть на полшага впереди наших партнёров, коллег, конкурентов».</span>
                  <span class="people-slider__author">Генеральный директор «Газпромнефть-Аэро»<br/>Владимир Егоров</span>
                </div>
              </div>
            </div>
            <div class="swiper-slide" style="background-image:url(/images/people-slider/1.jpg); background-size: cover;  background-position: center;">
              <div class="row">
                <div class="col-md-18 col-sm-20 col-xs-20">
                  <span class="people-slider__rubric">Люди дела</span>
                  <span class="people-slider__title">«Наша философия в том, что всегда надо быть на полшага впереди наших партнёров, коллег, конкурентов».</span>
                  <span class="people-slider__author">Генеральный директор «Газпромнефть-Аэро»<br/>Владимир Егоров</span>
                </div>
              </div>
            </div>
            <div class="swiper-slide" style="background-image:url(/images/people-slider/1.jpg); background-size: cover; background-position: center;">
              <div class="row">
                <div class="col-md-18 col-sm-20 col-xs-20">
                  <span class="people-slider__rubric">Люди дела</span>
                  <span class="people-slider__title">«Наша философия в том, что всегда надо быть на полшага впереди наших партнёров, коллег, конкурентов».</span>
                  <span class="people-slider__author">Генеральный директор «Газпромнефть-Аэро»<br/>Владимир Егоров</span>
                </div>
              </div>
            </div>
            <div class="swiper-slide" style="background-image:url(/images/people-slider/1.jpg); background-size: cover;  background-position: center;">
              <div class="row">
                <div class="col-md-18 col-sm-20 col-xs-20">
                  <span class="people-slider__rubric">Люди дела</span>
                  <span class="people-slider__title">«Наша философия в том, что всегда надо быть на полшага впереди наших партнёров, коллег, конкурентов».</span>
                  <span class="people-slider__author">Генеральный директор «Газпромнефть-Аэро»<br/>Владимир Егоров</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="people-slider__thumbs people-thumbs">
          <div class="swiper-container people-thumbs__wrap">
            <div class="swiper-wrapper">
              <div class="swiper-slide" style="background-image:url(/images/people-slider/1.jpg); background-size: cover;"></div>
              <div class="swiper-slide" style="background-image:url(/images/people-slider/1.jpg); background-size: cover;"></div>
              <div class="swiper-slide" style="background-image:url(/images/people-slider/1.jpg); background-size: cover;"></div>
              <div class="swiper-slide" style="background-image:url(/images/people-slider/1.jpg); background-size: cover;"></div>
              <div class="swiper-slide" style="background-image:url(/images/people-slider/1.jpg); background-size: cover;"></div>
              <div class="swiper-slide" style="background-image:url(/images/people-slider/1.jpg); background-size: cover;"></div>
            </div>
          </div>
          <div class="people-thumbs__next"></div>
          <div class="people-thumbs__prev"></div>
        </div>
      </div>
    </div>
    <div class="career-wrap__container">
      <div class="row">
        <div class="col-md-30">
          <div class="swiper-container story-slider">
            <div class="swiper-wrapper">
              <div class="swiper-slide">
                <div class="row story-slider__wrap">
                  <div class="col-md-7 col-sm-8 col-xs-30">
                    <img class="story-slider__photo" src="/images/success_1.jpg" alt="Полина Иванова">
                  </div>
                  <div class="col-md-22 col-md-offset-1 col-sm-20 col-sm-offset-2 col-xs-30">
                    <p class="story-slider__rubric">Истории успеха</p>
                    <p class="story-slider__text">
                      Сейчас, когда я уже через многое на своей работе прошел, когда существует определенный багаж знаний, хочется передать его. И для меня важно поделиться опытом, чтобы те ребята, которые к нам сейчас приходят с горящими глазами после учебных заведений, чуть быстрее прошли весь длинный путь познания<span class="story-slider__brackets"></span>
                    </p>
                    <p class="story-slider__author">Юрий Сидельников</p>
                    <p class="story-slider__school">Оператор по добыче нефти и газа ОАО «Газпромнефть-Ноябрьскнефтегаз»</p>
                  </div>
                </div>
              </div>
              <div class="swiper-slide">
                <div class="row story-slider__wrap">
                  <div class="col-md-7 col-sm-8 col-xs-30">
                    <img class="story-slider__photo" src="/images/success_1.jpg" alt="Полина Иванова">
                  </div>
                  <div class="col-md-22 col-md-offset-1 col-sm-20 col-sm-offset-2 col-xs-30">
                    <p class="story-slider__rubric">Истории успеха</p>
                    <p class="story-slider__text">
                      Сейчас, когда я уже через многое на своей работе прошел, когда существует определенный багаж знаний, хочется передать его. И для меня важно поделиться опытом, чтобы те ребята, которые к нам сейчас приходят с горящими глазами после учебных заведений, чуть быстрее прошли весь длинный путь познания<span class="story-slider__brackets"></span>
                    </p>
                    <p class="story-slider__author">Юрий Сидельников</p>
                    <p class="story-slider__school">Оператор по добыче нефти и газа ОАО «Газпромнефть-Ноябрьскнефтегаз»</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="swiper-pagination story-slider__pagination"></div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="career-wrap career-wrap_news_list">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-30">
        <div class="news">
          <h2 class="news__title">Новости</h2>
          <ul class="news__list">
            <li class="news__item">
              <a href="#" class="news__name">«Газпром нефть» стала лучшим работодателем России</a>
              <span class="news__date">18 февраля 2016</span>
            </li>
            <li class="news__item">
              <a href="#" class="news__name">«Газпром нефть» заключила соглашение о сотрудничестве с Тюменским государственным университетом</a>
              <span class="news__date">18 февраля 2016</span>
            </li>
            <li class="news__item">
              <a href="#" class="news__name">«Газпром нефть» стала лучшим работодателем России</a>
              <span class="news__date">18 февраля 2016</span>
            </li>
          </ul>
          <a href="#" class="news__more">Показать еще</a>
        </div>
      </div>
    </div>
  </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
