<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->AddHeadScript('/js/story-slider.js');
$APPLICATION->AddHeadScript('/js/tabs-accordeon.js');
?>

<section class="career-header">
  <div class="career-header__wrap container-fluid">
    <div class="row">
      <div class="col-md-4 col-sm-8 col-xs-8">
        <a href="/career/" class="career-header__logo">Карьера</a>
      </div>
      <div class="col-md-22 col-sm-30 col-xs-30 mobile-nav">
        <nav class="career-header__nav career-nav">
          <ul class="career-nav__list">
            <li class="career-nav__item"><a class="career-nav__link career-nav__link_active" href="#">Профессионалам</a></li>
            <li class="career-nav__item"><a class="career-nav__link" href="#">Студентам и выпускникам</a></li>
            <li class="career-nav__item"><a class="career-nav__link" href="#">О работе в компании</a></li>
            <li class="career-nav__item"><a class="career-nav__link" href="#">Вакансии</a></li>
          </ul>
        </nav>
      </div>
      <div class="col-md-3 col-md-offset-1 col-md-3 col-md-offset-1 col-sm-6 col-sm-offset-16 col-xs-22">
        <input class="career-header__button career-button career-button_login" type="submit" name="login" value="Вход">
      </div>
    </div>
  </div>
</section>
<section class="career-banner career-banner_function">
  <div class="container-fluid career-banner__title">
    <div class="row">
      <div class="col-md-20 col-sm-20 col-xs-25">
        <ul class="inline-breadcrumbs">
          <a href="/career/professionals/" class="inline-breadcrumbs__item">Профессионалам</a>
          <li class="inline-breadcrumbs__item inline-breadcrumbs__item_active">Производственные и технические функции</li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col-md-15 col-sm-20 col-xs-30">
        <h1 class="career-h1 career-h1_professionals">Бурение и внутрискважинные работы</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-15 col-sm-20 col-xs-30">
        <a class="career-button career-button_watch" href="/career/vacancies/index.html">Смотреть вакансии</a>
      </div>
    </div>
  </div>
</section>
<section class="career-text">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-30 col-sm-30 col-xs-30">
        <strong class="career-text__title">
          Функция бурения и внутрискважинных работ «Газпром нефти» – одна из самых прогрессивных в российской энергетической отрасли.
        </strong>
        <p class="career-text__text">
          Мы выполняем уникальные проекты и осуществляем бурение в  различных точках земного шара – от Арктики до Ирака – и работаем в сложных климатических условиях, получая востребованные отраслью компетенции и знания. Наши сотрудники имеют возможность привлекать международный опыт и использовать самые современные отраслевые практики.
        </p>
        <p class="career-text__text">
          Мы тестируем и внедряем передовые технологии для работы с трудноизвлекаемыми запасами и способствуем разработке отечественного оборудования. Наша цель - повышать эффективность процессов, выводя компанию на новый технологический уровень по отношению к другим участникам отрасли. В то же время приоритетом компании остаётся забота об окружающей среде и безопасности.
        </p>
        <p class="career-text__text">
          Мы создаем единую базу знаний для обмена опытом. С нами вы сможете реализовать новые идеи, постоянно расширяя области своей экспертизы. Наши сотрудники развиваются в своей профессиональной области по индивидуальным программам, а также становятся внутренними тренерами и наставниками для коллег.
        </p>
      </div>
    </div>
  </div>
</section>

<section class="career-steps career-steps_professionals">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-30 col-sm-30 col-xs-30">
        <strong class="career-steps__title">С чего начать? Три простых шага</strong>
      </div>
    </div>
    <div class="row">
      <div class="col-md-30 col-sm-30 col-xs-30">
        <ul class="career-steps__list">
          <li class="career-steps__item career-steps__item_reg"><a class="career-steps__link" href="#">Зарегистрируйся</a></li>
          <li class="career-steps__item career-steps__item_load"><a class="career-steps__link" href="#">Загрузи резюме</a></li>
          <li class="career-steps__item career-steps__item_vacancy"><a class="career-steps__link" href="#">Откликнись на вакансию</a></li>
        </ul>
      </div>
    </div>
  </div>
</section>
<section class="career-specialists">
  <div class="container-fluid">
    <div class="career-wrap__container">
      <div class="row">
        <div class="col-md-30">
          <div class="swiper-container story-slider swiper-container-horizontal">
            <div class="swiper-wrapper" style="transform: translate3d(0px, 0px, 0px); transition-duration: 0ms;">
              <div class="swiper-slide swiper-slide-active" style="width: 980px;">
                <div class="row story-slider__wrap">
                  <div class="col-md-7 col-sm-8 col-xs-30">
                    <img class="story-slider__photo" src="/images/success_1.jpg" alt="Полина Иванова">
                  </div>
                  <div class="col-md-22 col-md-offset-1 col-sm-19 col-sm-offset-3  col-xs-30">
                    <p class="story-slider__rubric">Истории успеха</p>
                    <p class="story-slider__text">
                      Сейчас, когда я уже через многое на своей работе прошел, когда существует определенный багаж знаний, хочется передать его. И для меня важно поделиться опытом, чтобы те ребята, которые к нам сейчас приходят с горящими глазами после учебных заведений, чуть быстрее прошли весь длинный путь познания<span class="story-slider__brackets"></span>
                    </p>
                    <p class="story-slider__author">Юрий Сидельников</p>
                    <p class="story-slider__school">Оператор по добыче нефти и газа ОАО «Газпромнефть-Ноябрьскнефтегаз»</p>
                  </div>
                </div>
              </div>
              <div class="swiper-slide swiper-slide-next" style="width: 980px;">
                <div class="row story-slider__wrap">
                  <div class="col-md-7 col-sm-8 col-xs-30">
                    <img class="story-slider__photo" src="/images/success_1.jpg" alt="Полина Иванова">
                  </div>
                  <div class="col-md-22 col-md-offset-1 col-sm-19 col-sm-offset-3 col-xs-30">
                    <p class="story-slider__rubric">Истории успеха</p>
                    <p class="story-slider__text">
                      Сейчас, когда я уже через многое на своей работе прошел, когда существует определенный багаж знаний, хочется передать его. И для меня важно поделиться опытом, чтобы те ребята, которые к нам сейчас приходят с горящими глазами после учебных заведений, чуть быстрее прошли весь длинный путь познания<span class="story-slider__brackets"></span>
                    </p>
                    <p class="story-slider__author">Юрий Сидельников</p>
                    <p class="story-slider__school">Оператор по добыче нефти и газа ОАО «Газпромнефть-Ноябрьскнефтегаз»</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="swiper-pagination story-slider__pagination swiper-pagination-clickable swiper-pagination-bullets"><span class="swiper-pagination-bullet swiper-pagination-bullet-active"></span><span class="swiper-pagination-bullet"></span></div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="career-vacancies">
  <div class="container-fluid">
    <div class="row career-vacancies__header">
      <div class="col-md-30 col-sm-30 col-xs-30">
        <strong class="career-vacancies__title">Новые вакансии</strong>
      </div>
    </div>
    <div class="row">
      <div class="col-md-30 col-sm-30 col-xs-30">
        <div class="career-vacancies__list vacancies-list vacancies-list_spec">
          <div class="vacancies-list__item">
            <div class="row">
              <div class="col-md-22 col-sm-22 col-xs-30">
                <a href="" class="vacancies-list__title">Ведущий инженер АСУТП</a>
                <span class="vacancies-list__type">Маркетинг</span>
              </div>
              <div class="col-md-6 col-md-offset-2 col-sm-4 col-sm-offset-4 col-xs-30 vacancies-list__info">
                <span class="vacancies-list__city">Омск</span>
                <span class="vacancies-list__date">11 декабря 2016</span>
              </div>
            </div>
          </div>
          <div class="vacancies-list__item">
            <div class="row">
              <div class="col-md-22 col-sm-22 col-xs-30">
                <a href="" class="vacancies-list__title">Ведущий инженер АСУТП</a>
                <span class="vacancies-list__type">Маркетинг</span>
              </div>
              <div class="col-md-6 col-md-offset-2 col-sm-4 col-sm-offset-4 col-xs-30">
                <span class="vacancies-list__city">Омск</span>
                <span class="vacancies-list__date">11 декабря 2016</span>
              </div>
            </div>
          </div>
          <div class="vacancies-list__item">
            <div class="row">
              <div class="col-md-22 col-sm-22 col-xs-30">
                <a href="" class="vacancies-list__title">Ведущий инженер АСУТП</a>
                <span class="vacancies-list__type">Маркетинг</span>
              </div>
              <div class="col-md-6 col-md-offset-2 col-sm-4 col-sm-offset-4 col-xs-30">
                <span class="vacancies-list__city">Омск</span>
                <span class="vacancies-list__date">11 декабря 2016</span>
              </div>
            </div>
          </div>
          <div class="vacancies-list__item">
            <div class="row">
              <div class="col-md-22 col-sm-22 col-xs-30">
                <a href="" class="vacancies-list__title">Ведущий инженер АСУТП</a>
                <span class="vacancies-list__type">Маркетинг</span>
              </div>
              <div class="col-md-6 col-md-offset-2 col-sm-4 col-sm-offset-4 col-xs-30">
                <span class="vacancies-list__city">Омск</span>
                <span class="vacancies-list__date">11 декабря 2016</span>
              </div>
            </div>
          </div>
          <a href="" class="vacancies-list__more">Загрузить еще</a>
        </div>
      </div>
    </div>
  </div>
</section>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>
