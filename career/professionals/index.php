<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->AddHeadScript('/js/story-slider.js');
$APPLICATION->AddHeadScript('/js/tabs-accordeon.js');
?>

<section class="career-header">
  <div class="career-header__wrap container-fluid">
    <div class="row">
      <div class="col-md-4 col-sm-8 col-xs-8">
        <a href="/career/" class="career-header__logo">Карьера</a>
      </div>
      <div class="col-md-22 col-sm-30 col-xs-30 mobile-nav">
        <nav class="career-header__nav career-nav">
          <ul class="career-nav__list">
            <li class="career-nav__item"><a class="career-nav__link career-nav__link_active" href="#">Профессионалам</a></li>
            <li class="career-nav__item"><a class="career-nav__link" href="#">Студентам и выпускникам</a></li>
            <li class="career-nav__item"><a class="career-nav__link" href="#">О работе в компании</a></li>
            <li class="career-nav__item"><a class="career-nav__link" href="#">Вакансии</a></li>
          </ul>
        </nav>
      </div>
      <div class="col-md-3 col-md-offset-1 col-md-3 col-md-offset-1 col-sm-6 col-sm-offset-16 col-xs-22">
        <input class="career-header__button career-button career-button_login" type="submit" name="login" value="Вход">
      </div>
    </div>
  </div>
</section>
<section class="career-banner career-banner_professionals">
  <div class="container-fluid career-banner__title">
    <div class="row">
      <div class="col-md-15 col-sm-20 col-xs-30">
        <h1 class="career-h1 career-h1_professionals">Профессионалам</h1>
        <h2 class="career-h2 career-h2_professionals">большие возможности развития</h2>
      </div>
    </div>
  </div>
</section>
<section class="career-text">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-30 col-sm-30 col-xs-30">
        <strong class="career-text__title">
          Работая с нами, ты создаёшь результат, которым сможешь по праву гордиться!
        </strong>
        <p class="career-text__text">
          Наша команда профессионалов ежедневно участвует в решении интересных задач, для чего мы создаем и применяем новые подходы. Мы растём, следуя за динамикой бизнеса компании, получая возможность раскрыть свой потенциал.
        </p>
        <p class="career-text__text">
          Мы реализуем себя и достигаем впечатляющих результатов, участвуя в воплощении амбициозных, зачастую уникальных проектов компании как в России, так и за рубежом.<br/>
          Вместе с коллективом единомышленников мы работаем на будущее, которым сможем по праву гордиться.
        </p>
        <p class="career-text__text">
          Стань частью нашей команды – создай личный кабинет, загрузи резюме, расскажи о своём опыте, знаниях, навыках и успехах. Подписывайся и откликайся на вакансии, и в случае интереса с нашей стороны мы свяжемся с тобой.
        </p>
      </div>
    </div>
  </div>
</section>
<section class="career-choose">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-30 col-sm-30 col-xs-30">
        <strong class="career-choose__title">Выберите профессиональную область</strong>
      </div>
    </div>
    <div class="career-choose__tabs tabs-accordeon">
      <div class="row">
        <div class="col-md-9 col-sm-9 col-xs-30">
          <span data-tab="1" class="tabs-accordeon__nav-item active">Производственные и технические функции</span>
        </div>
        <div class="col-md-19 col-md-offset-2 col-sm-18 col-sm-offset-2 col-xs-30 career-choose__col">
          <ul data-content="1" class="tabs-accordeon__content-item active">
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Геология</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Добыча</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Газ</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Бурение и внутрискважные работы</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Инжиниринг, реинжиниринг</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Логистика и транспорт</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Производственная безопасность</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Капитальное строительство</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Нефтепереработка</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Проектные сервисы</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Энергетика</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Развитие технологий</a></li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col-md-9 col-sm-9 col-xs-30">
          <span data-tab="2" class="tabs-accordeon__nav-item">Реализация нефти и нефтепродуктов</span>
        </div>
        <div class="col-md-19 col-md-offset-2 col-sm-18 col-sm-offset-2 col-xs-30 career-choose__col">
          <ul data-content="2" class="tabs-accordeon__content-item">
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Геология</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Добыча</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Газ</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Бурение и внутрискважные работы</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Инжиниринг, реинжиниринг</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Логистика и транспорт</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Производственная безопасность</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Капитальное строительство</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Нефтепереработка</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Проектные сервисы</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Энергетика</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Развитие технологий</a></li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col-md-9 col-sm-9 col-xs-30">
          <span data-tab="3" class="tabs-accordeon__nav-item">Корпоративные функции</span>
        </div>
        <div class="col-md-19 col-md-offset-2 col-sm-18 col-sm-offset-2 col-xs-30 career-choose__col">
          <ul data-content="3" class="tabs-accordeon__content-item">
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Геология</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Добыча</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Газ</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Бурение и внутрискважные работы</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Инжиниринг, реинжиниринг</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Логистика и транспорт</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Производственная безопасность</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Капитальное строительство</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Нефтепереработка</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Проектные сервисы</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Энергетика</a></li>
            <li class="tabs-accordeon__tag"><a href="/career/professionals/function/detail.php">Развитие технологий</a></li>
          </ul>
        </div>
      </div>

    </div>
  </div>
</section>
<section class="career-steps career-steps_professionals">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-30 col-sm-30 col-xs-30">
        <strong class="career-steps__title">С чего начать? Три простых шага</strong>
      </div>
    </div>
    <div class="row">
      <div class="col-md-30 col-sm-30 col-xs-30">
        <ul class="career-steps__list">
          <li class="career-steps__item career-steps__item_reg"><a class="career-steps__link" href="#">Зарегистрируйся</a></li>
          <li class="career-steps__item career-steps__item_load"><a class="career-steps__link" href="#">Загрузи резюме</a></li>
          <li class="career-steps__item career-steps__item_vacancy"><a class="career-steps__link" href="#">Откликнись на вакансию</a></li>
        </ul>
      </div>
    </div>
  </div>
</section>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>
