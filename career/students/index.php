<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->AddHeadScript('/js/story-slider.js');
?>

<section class="career-header">
  <div class="career-header__wrap container-fluid">
    <div class="row">
      <div class="col-md-4 col-sm-8 col-xs-8">
        <a href="/career/" class="career-header__logo">Карьера</a>
      </div>
      <div class="col-md-22 col-sm-30 col-xs-30 mobile-nav">
        <nav class="career-header__nav career-nav">
          <ul class="career-nav__list">
            <li class="career-nav__item"><a class="career-nav__link" href="#">Профессионалам</a></li>
            <li class="career-nav__item"><a class="career-nav__link" href="#">Студентам и выпускникам</a></li>
            <li class="career-nav__item"><a class="career-nav__link" href="#">О работе в компании</a></li>
            <li class="career-nav__item"><a class="career-nav__link" href="#">Вакансии</a></li>
          </ul>
        </nav>
      </div>
      <div class="col-md-3 col-md-offset-1 col-md-3 col-md-offset-1 col-sm-6 col-sm-offset-16 col-xs-22">
        <input class="career-header__button career-button career-button_login" type="submit" name="login" value="Вход">
      </div>
    </div>
  </div>
</section>
<section class="career-banner career-banner_students">
  <div class="container-fluid career-banner__title">
    <div class="row">
      <div class="col-md-15 col-sm-20 col-xs-30">
        <h1 class="career-h1 career-h1_students">Студентам и выпускникам:</h1>
        <h2 class="career-h2">ты готов к решению глобальных задач?</h2>
      </div>
    </div>
  </div>
</section>
<section class="career-text">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-30 col-sm-30 col-xs-30">
        <strong class="career-text__title">
          Старт карьеры в «Газпром нефти» - это залог твоего будущего успеха и становления как профессионала нефтегазовой отрасли.
        </strong>
        <strong class="career-text__title">Студентам</strong>
        <p class="career-text__text">
          Программы стажировок «Газпром нефти» — это открытые возможности для развития профессиональных и лидерских качеств. Стажировки проходят на производстве, где студенты работают c масштабными проектами и решают практические задачи вместе c опытными профессионалами. Работа на добывающих и перерабатывающих предприятиях в крупнейших нефтегазовых регионах России и за рубежом — это возможность взглянуть на мир по-новому и дать старт своей карьере. Заяви о себе на нашем портале – создай личный кабинет, заполни резюме и расскажи о себе максимально подробно. Подпишись на вакансии, следи за программами стажировок в своём ВУЗе, участвуй в мероприятиях для студентов – и ты получишь шанс пройти.
        </p>
        <strong class="career-text__title">Выпускникам</strong>
        <p class="career-text__text">
          Для молодых специалистов мы реализуем трехлетнюю программу развития корпоративных и профессиональных компетенций сотрудников. Участие в программе под руководством опытного наставника — гарантия динамичного карьерного старта в нефтегазовой отрасли. С первых дней работы ты погрузишься в решение интересных задач, примешь участие в крупнейших проектах и сможешь предложить новые подходы и эффективные решения, применяя на практике знания, полученные за время обучения. Приходя на работу к нам в рамках программы для молодых специалистов, ты можешь быть уверен, что получишь равные со своими старшими коллегами возможности для развития и реализации в профессии и личностно. Мы ждём тебя – создай резюме в личном кабинете, подпишись на вакансии и твой старт карьеры будет не за горами.
        </p>
      </div>
    </div>
  </div>
</section>
<section class="career-steps">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-30 col-sm-30 col-xs-30">
        <strong class="career-steps__title">С чего начать? Три простых шага</strong>
      </div>
    </div>
    <div class="row">
      <div class="col-md-30 col-sm-30 col-xs-30">
        <ul class="career-steps__list">
          <li class="career-steps__item career-steps__item_reg"><a class="career-steps__link" href="#">Зарегистрируйся</a></li>
          <li class="career-steps__item career-steps__item_load"><a class="career-steps__link" href="#">Загрузи резюме</a></li>
          <li class="career-steps__item career-steps__item_vacancy"><a class="career-steps__link" href="#">Откликнись на вакансию</a></li>
        </ul>
      </div>
    </div>
  </div>
</section>
<section class="career-specialists">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-30 col-sm-30 col-xs-30">
        <strong class="career-specialists__title">Наши молодые специалисты</strong>
      </div>
    </div>
    <div class="swiper-container story-slider">
      <div class="swiper-wrapper">
        <div class="swiper-slide">
          <div class="row story-slider__wrap">
            <div class="col-md-7 col-sm-8 col-xs-30">
              <img class="story-slider__photo" src="/images/spec_1.jpg" alt="Полина Иванова">
            </div>
            <div class="col-md-22 col-md-offset-1 col-sm-19 col-sm-offset-3 col-xs-30">
              <p class="story-slider__text">
                Это мероприятие оказалось очень ярким, увлекательным и волнительным! Очень понравилась общая экскурсия. Турнир дал мне новые знания по нефтегазовой отрасли, а также повысил мой интерес к науке.» Классно, что в задании нет верного ответа. Такой формат готовит настоящих ученых, перед которыми стоят еще нерешенные задачи<span class="story-slider__brackets"></span>
              </p>
              <p class="story-slider__author">Полина Иванова</p>
              <p class="story-slider__school">Санкт-Петербург, 95 лицей</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="career-vacancies">
  <div class="container-fluid">
    <div class="row career-vacancies__header">
      <div class="col-md-30 col-sm-30 col-xs-30">
        <strong class="career-vacancies__title">Новые вакансии</strong>
      </div>
    </div>
    <div class="row">
      <div class="col-md-30 col-sm-30 col-xs-30">
        <div class="career-vacancies__list vacancies-list vacancies-list_spec">
          <div class="vacancies-list__item">
            <div class="row">
              <div class="col-md-22 col-sm-22 col-xs-30">
                <a href="" class="vacancies-list__title">Ведущий инженер АСУТП</a>
                <span class="vacancies-list__type">Маркетинг</span>
              </div>
              <div class="col-md-6 col-md-offset-2 col-sm-4 col-sm-offset-4 col-xs-30 vacancies-list__info">
                <span class="vacancies-list__city">Омск</span>
                <span class="vacancies-list__date">11 декабря 2016</span>
              </div>
            </div>
          </div>
          <div class="vacancies-list__item">
            <div class="row">
              <div class="col-md-22 col-sm-22 col-xs-30">
                <a href="" class="vacancies-list__title">Ведущий инженер АСУТП</a>
                <span class="vacancies-list__type">Маркетинг</span>
              </div>
              <div class="col-md-6 col-md-offset-2 col-sm-4 col-sm-offset-4 col-xs-30">
                <span class="vacancies-list__city">Омск</span>
                <span class="vacancies-list__date">11 декабря 2016</span>
              </div>
            </div>
          </div>
          <div class="vacancies-list__item">
            <div class="row">
              <div class="col-md-22 col-sm-22 col-xs-30">
                <a href="" class="vacancies-list__title">Ведущий инженер АСУТП</a>
                <span class="vacancies-list__type">Маркетинг</span>
              </div>
              <div class="col-md-6 col-md-offset-2 col-sm-4 col-sm-offset-4 col-xs-30">
                <span class="vacancies-list__city">Омск</span>
                <span class="vacancies-list__date">11 декабря 2016</span>
              </div>
            </div>
          </div>
          <div class="vacancies-list__item">
            <div class="row">
              <div class="col-md-22 col-sm-22 col-xs-30">
                <a href="" class="vacancies-list__title">Ведущий инженер АСУТП</a>
                <span class="vacancies-list__type">Маркетинг</span>
              </div>
              <div class="col-md-6 col-md-offset-2 col-sm-4 col-sm-offset-4 col-xs-30">
                <span class="vacancies-list__city">Омск</span>
                <span class="vacancies-list__date">11 декабря 2016</span>
              </div>
            </div>
          </div>
          <a href="" class="vacancies-list__more">Загрузить еще</a>
        </div>
      </div>
    </div>
  </div>
</section>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>
