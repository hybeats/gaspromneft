<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>

<section class="career-header">
  <div class="career-header__wrap container-fluid">
    <div class="row">
      <div class="col-md-4 col-sm-8 col-xs-8">
        <a href="/career/" class="career-header__logo">Карьера</a>
      </div>
      <div class="col-md-22 col-sm-30 col-xs-30 mobile-nav">
        <nav class="career-header__nav career-nav">
          <ul class="career-nav__list">
            <li class="career-nav__item"><a class="career-nav__link" href="#">Профессионалам</a></li>
            <li class="career-nav__item"><a class="career-nav__link" href="#">Студентам и выпускникам</a></li>
            <li class="career-nav__item"><a class="career-nav__link" href="#">О работе в компании</a></li>
            <li class="career-nav__item"><a class="career-nav__link" href="#">Вакансии</a></li>
          </ul>
        </nav>
      </div>
      <div class="col-md-3 col-md-offset-1 col-md-3 col-md-offset-1 col-sm-6 col-sm-offset-16 col-xs-22">
        <input class="career-header__button career-button career-button_login" type="submit" name="login" value="Вход">
      </div>
    </div>
  </div>
</section>
<section class="career-vacancy">
  <div class="career-vacancy__wrap container-fluid">
    <span class="career-vacancy__type">Нефтепереработка</span>
    <h2 class="career-vacancy__name">Ведущий инженер АСУТП</h1>
    <div class="career-vacancy__info">
      <span class="career-vacancy__city">Омск</span>
      <span class="career-vacancy__date">11 декабря 2016</span>
    </div>
    <div class="row career-vacancy__detail">
      <div class="col-md-18 col-sm-18 col-xs-30">
        <div class="career-vacancy__block">
          <h3 class="career-vacancy__title">Обязанности:</h3>
          <p class="career-vacancy__text">Осуществление технического обслуживания систем промышленной автоматизации на технологических объектах.</p>
        </div>
        <div class="career-vacancy__block">
          <h3 class="career-vacancy__title">Требования:</h3>
          <ul class="career-vacancy__list">
            <li class="career-vacancy__item">Высшее техническое образование;</li>
            <li class="career-vacancy__item">Опыт работы с системами промышленной автоматизации: Centrum, Pro-Safe, Stardom, Siemens, Delta V, Allen-Bradley,
Modicon, Industrial IT</li>
            <li class="career-vacancy__item">Умение работать с документацией;</li>
            <li class="career-vacancy__item">Установка операционных систем;</li>
            <li class="career-vacancy__item">Настройка дополнительного оборудования;</li>
            <li class="career-vacancy__item">Английский (со словарем).</li>
          </ul>
        </div>
        <div class="career-vacancy__block">
          <h3 class="career-vacancy__title">Условия:</h3>
          <ul class="career-vacancy__list">
            <li class="career-vacancy__item">Официальное трудоустройство, соцпакет</li>
            <li class="career-vacancy__item">Окончательные условия по заработной плате будут установлены по результатам собеседования.</li>
          </ul>
        </div>
      </div>
      <div class="col-md-10 col-md-offset-2 col-sm-10 col-sm-offset-2 col-xs-30">
        <div class="career-vacancy__block career-vacancy__block_side">
          <h3 class="career-vacancy__title">Тип занятости:</h3>
          <p class="career-vacancy__text">Полная занятость, полный день</p>
        </div>
        <div class="career-vacancy__block career-vacancy__block_side">
          <h3 class="career-vacancy__title">Требуемый опыт:</h3>
          <p class="career-vacancy__text">от 1 года до 3 лет</p>
        </div>
      </div>
    </div>
    <a href="" class="career-vacancy__callback">Откликнуться на вакансию</a>
  </div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
