<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Газпром Нефть");

$APPLICATION->SetAdditionalCSS("/js/plugins/icheck/icheck.css");

$APPLICATION->AddHeadScript('/js/search-input-clear.js');
$APPLICATION->AddHeadScript('/js/delete-tag.js');
$APPLICATION->AddHeadScript('/js/plugins/icheck/icheck.min.js');
$APPLICATION->AddHeadScript('/js/vacancy-filter.js');

?>
<section class="career-header">
  <div class="career-header__wrap container-fluid">
    <div class="row">
      <div class="col-md-4 col-sm-8 col-xs-8">
        <a href="/career/" class="career-header__logo">Карьера</a>
      </div>
      <div class="col-md-22 col-sm-30 col-xs-30 mobile-nav">
        <nav class="career-header__nav career-nav">
          <ul class="career-nav__list">
            <li class="career-nav__item"><a class="career-nav__link" href="#">Профессионалам</a></li>
            <li class="career-nav__item"><a class="career-nav__link" href="#">Студентам и выпускникам</a></li>
            <li class="career-nav__item"><a class="career-nav__link" href="#">О работе в компании</a></li>
            <li class="career-nav__item"><a class="career-nav__link" href="#">Вакансии</a></li>
          </ul>
        </nav>
      </div>
      <div class="col-md-3 col-md-offset-1 col-md-3 col-md-offset-1 col-sm-6 col-sm-offset-16 col-xs-22">
        <input class="career-header__button career-button career-button_login" type="submit" name="login" value="Вход">
      </div>
    </div>
  </div>
</section>
<section class="career-banner career-banner_vacancies">
  <div class="container-fluid career-banner__title">
    <div class="row">
      <div class="col-md-30 col-sm-30 col-xs-30">
        <h1 class="career-h1 career-h1_vacancies">Вакансии в компании<br/>«Газпром Нефть»</h1>
        <form class="search-form" action="" method="post">
          <div class="search-form__row">
            <input class="search-form__input" type="text" name="" value="" placeholder="Искать вакансии">
            <input class="search-form__submit" type="submit" name="search-submit" value="">
            <span class="search-form__clear"></span>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<section class="career-tags">
  <div class="container-fluid">
    <div class="row">
      <ul class="career-tags__list col-md-30 col-sm-30 col-xs-30">
        <li class="career-tags__item">Санкт-Петербург<span class="career-tags__delete"></span></li>
        <li class="career-tags__item">Логистика и транспорт<span class="career-tags__delete"></span></li>
        <li class="career-tags__item">Последние 30 дней<span class="career-tags__delete"></span></li>
        <a href="" class="career-tags__more">показать все</a>
      </ul>
    </div>
  </div>
</section>
<section class="career-vacancies">
  <div class="container-fluid">
    <div class="career-vacanies__info">
      <p class="career-vacancies__count">Найдено: <span>20</span> вакансий<a class="career-vacancies__reset" href="#">сбросить</a></p>
    </div>
    <a href="" class="career-vacancies__filter-btn">фильтр</a>
    <div class="row">
      <div class="col-md-20 col-sm-30">
        <div class="career-vacancies__list vacancies-list">
          <div class="vacancies-list__item">
            <a href="" class="vacancies-list__title">Ведущий инженер АСУТП</a>
            <div class="vacancies-list__info">
              <span class="vacancies-list__city">Омск</span>
              <span class="vacancies-list__date">11 декабря 2016</span>
            </div>
          </div>
          <div class="vacancies-list__item">
            <a href="" class="vacancies-list__title">Главный специалист по эксплуатации АЗС Ведущий специалист по энергетике</a>
            <div class="vacancies-list__info">
              <span class="vacancies-list__city">Омск</span>
              <span class="vacancies-list__date">11 декабря 2016</span>
            </div>
          </div>
          <div class="vacancies-list__item">
            <a href="" class="vacancies-list__title">Ведущий инженер АСУТП</a>
            <div class="vacancies-list__info">
              <span class="vacancies-list__city">Омск</span>
              <span class="vacancies-list__date">11 декабря 2016</span>
            </div>
          </div>
          <div class="vacancies-list__item">
            <a href="" class="vacancies-list__title">Ведущий инженер АСУТП</a>
            <div class="vacancies-list__info">
              <span class="vacancies-list__city">Омск</span>
              <span class="vacancies-list__date">11 декабря 2016</span>
            </div>
          </div>
          <div class="vacancies-list__item">
            <a href="" class="vacancies-list__title">Ведущий инженер АСУТП</a>
            <div class="vacancies-list__info">
              <span class="vacancies-list__city">Омск</span>
              <span class="vacancies-list__date">11 декабря 2016</span>
            </div>
          </div>
          <div class="vacancies-list__item">
            <a href="" class="vacancies-list__title">Ведущий инженер АСУТП</a>
            <div class="vacancies-list__info">
              <span class="vacancies-list__city">Омск</span>
              <span class="vacancies-list__date">11 декабря 2016</span>
            </div>
          </div>
          <div class="vacancies-list__item">
            <a href="" class="vacancies-list__title">Ведущий инженер АСУТП</a>
            <div class="vacancies-list__info">
              <span class="vacancies-list__city">Омск</span>
              <span class="vacancies-list__date">11 декабря 2016</span>
            </div>
          </div>
          <a href="" class="vacancies-list__more">Загрузить еще</a>
        </div>
      </div>
      <div class="col-md-8 col-md-offset-2 col-sm-15 mobile-vac-filter">
        <div class="career-vacancies__filter vacancies-filter">
          <form class="vacancies-filter__form" action="index.html" method="post">
            <input class="vacancies-filter__submit" type="submit" name="" value="применить">
            <span class="vacancies-filter__close"></span>
            <div class="vacancies-filter__item">
              <strong class="vacancies-filter__title">Регион</strong>
              <div class="vacancies-filter__container">
                <div class="vacancies-filter__row">
                  <label class="vacancies-filter__label">
                    <input class="vacancies-filter__checkbox" type="checkbox" name="" value="">
                    <span>Республика Адыгея</span>
                  </label>
                </div>
                <div class="vacancies-filter__row">
                  <label class="vacancies-filter__label">
                    <input class="vacancies-filter__checkbox" type="checkbox" name="" value="">
                    <span>Республика Башкортостан</span>
                  </label>
                </div>
                <div class="vacancies-filter__row">
                  <label class="vacancies-filter__label">
                    <input class="vacancies-filter__checkbox" type="checkbox" name="" value="">
                    <span>Республика Бурятия</span>
                  </label>
                </div>
                <div class="vacancies-filter__row">
                  <label class="vacancies-filter__label">
                    <input class="vacancies-filter__checkbox" type="checkbox" name="" value="">
                    <span>Республика Алтай</span>
                  </label>
                </div>
                <div class="vacancies-filter__row">
                  <label class="vacancies-filter__label">
                    <input class="vacancies-filter__checkbox" type="checkbox" name="" value="">
                    <span>Республика Дагестан</span>
                  </label>
                </div>
                <a href="#" class="vacancies-filter__more">все регионы</a>
              </div>
            </div>
            <div class="vacancies-filter__item">
              <strong class="vacancies-filter__title">Город</strong>
              <div class="vacancies-filter__container">
                <div class="vacancies-filter__row">
                  <label class="vacancies-filter__label">
                    <input class="vacancies-filter__checkbox" type="checkbox" name="" value="">
                    <span>Москва</span>
                  </label>
                </div>
                <div class="vacancies-filter__row">
                  <label class="vacancies-filter__label">
                    <input class="vacancies-filter__checkbox" type="checkbox" name="" value="">
                    <span>Санкт-Петербург</span>
                  </label>
                </div>
                <div class="vacancies-filter__row">
                  <label class="vacancies-filter__label">
                    <input class="vacancies-filter__checkbox" type="checkbox" name="" value="">
                    <span>Омск</span>
                  </label>
                </div>
                <div class="vacancies-filter__row">
                  <label class="vacancies-filter__label">
                    <input class="vacancies-filter__checkbox" type="checkbox" name="" value="">
                    <span>Новосибирск</span>
                  </label>
                </div>
                <div class="vacancies-filter__row">
                  <label class="vacancies-filter__label">
                    <input class="vacancies-filter__checkbox" type="checkbox" name="" value="">
                    <span>Тюмень</span>
                  </label>
                </div>
                <a href="#" class="vacancies-filter__more">все города</a>
              </div>
            </div>
            <div class="vacancies-filter__item">
              <strong class="vacancies-filter__title">Направление</strong>
              <div class="vacancies-filter__container">
                <div class="vacancies-filter__row">
                  <label class="vacancies-filter__label">
                    <input class="vacancies-filter__checkbox" type="checkbox" name="" value="">
                    <span>Производственные и технические функции</span>
                  </label>
                </div>
                <div class="vacancies-filter__row">
                  <label class="vacancies-filter__label">
                    <input class="vacancies-filter__checkbox" type="checkbox" name="" value="">
                    <span>Реализация нефти и нефтепродуктов</span>
                  </label>
                </div>
                <div class="vacancies-filter__row">
                  <label class="vacancies-filter__label">
                    <input class="vacancies-filter__checkbox" type="checkbox" name="" value="">
                    <span>Корпоративные функции</span>
                  </label>
                </div>
              </div>
            </div>
            <div class="vacancies-filter__item">
              <strong class="vacancies-filter__title">Функции</strong>
              <div class="vacancies-filter__container">
                <div class="vacancies-filter__row">
                  <label class="vacancies-filter__label">
                    <input class="vacancies-filter__checkbox" type="checkbox" name="" value="">
                    <span>Геология и Лицензирование</span>
                  </label>
                </div>
                <div class="vacancies-filter__row">
                  <label class="vacancies-filter__label">
                    <input class="vacancies-filter__checkbox" type="checkbox" name="" value="">
                    <span>Бурение и ВСР</span>
                  </label>
                </div>
                <div class="vacancies-filter__row">
                  <label class="vacancies-filter__label">
                    <input class="vacancies-filter__checkbox" type="checkbox" name="" value="">
                    <span>Добыча</span>
                  </label>
                </div>
                <div class="vacancies-filter__row">
                  <label class="vacancies-filter__label">
                    <input class="vacancies-filter__checkbox" type="checkbox" name="" value="">
                    <span>Газ</span>
                  </label>
                </div>
                <div class="vacancies-filter__row">
                  <label class="vacancies-filter__label">
                    <input class="vacancies-filter__checkbox" type="checkbox" name="" value="">
                    <span>Нефтепереработка</span>
                  </label>
                </div>
                <a href="#" class="vacancies-filter__more">все функции</a>
              </div>
            </div>
            <div class="vacancies-filter__item">
              <strong class="vacancies-filter__title">Тип занятости</strong>
              <div class="vacancies-filter__container">
                <div class="vacancies-filter__row">
                  <label class="vacancies-filter__label">
                    <input class="vacancies-filter__checkbox" type="checkbox" name="" value="">
                    <span>Профессионалы</span>
                  </label>
                </div>
                <div class="vacancies-filter__row">
                  <label class="vacancies-filter__label">
                    <input class="vacancies-filter__checkbox" type="checkbox" name="" value="">
                    <span>Студенты и выпускники</span>
                  </label>
                </div>
              </div>
            </div>
            <div class="vacancies-filter__item">
              <strong class="vacancies-filter__title">Размещено за последние</strong>
              <div class="vacancies-filter__container">
                <div class="vacancies-filter__row">
                  <label class="vacancies-filter__label">
                    <input class="vacancies-filter__checkbox" type="radio" name="radio1" value="">
                    <span>5 дней</span>
                  </label>
                </div>
                <div class="vacancies-filter__row">
                  <label class="vacancies-filter__label">
                    <input class="vacancies-filter__checkbox" type="radio" name="radio1" value="">
                    <span>10 дней</span>
                  </label>
                </div>
                <div class="vacancies-filter__row">
                  <label class="vacancies-filter__label">
                    <input class="vacancies-filter__checkbox" type="radio" name="radio1" value="">
                    <span>30 дней</span>
                  </label>
                </div>
                <div class="vacancies-filter__row">
                  <label class="vacancies-filter__label">
                    <input class="vacancies-filter__checkbox" type="radio" name="radio1" value="">
                    <span>60 дней</span>
                  </label>
                </div>
              </div>
            </div>
            <input class="vacancies-filter__submit" type="submit" name="" value="применить">
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
