$(document).ready(function() {
  var galleryTop = new Swiper('.people-slider__main', {
        spaceBetween: 0,
        slidesPerView: 1
    });
    var galleryThumbs = new Swiper('.people-thumbs__wrap', {
        spaceBetween: 0,
        slidesPerView: 5,
        touchRatio: 0.2,
        centeredSlides: true,
        slideToClickedSlide: true,
        nextButton: '.people-thumbs__next',
        prevButton: '.people-thumbs__prev',

    });
    galleryTop.params.control = galleryThumbs;
    galleryThumbs.params.control = galleryTop;
});
