var windowWidth = document.documentElement.clientWidth;

$(document).ready(function(){
  if (windowWidth < 1025) {
    $('.career-header__logo').click(function(e){
      e.preventDefault();
      $(this).toggleClass('active');
      $('.career-header').find('.mobile-nav').toggleClass('active');
    });
  }
});
