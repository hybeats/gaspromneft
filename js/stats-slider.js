$(document).ready(function() {
  var galleryTop = new Swiper('.stats-slider', {
    slidesPerView: 3,
    simulateTouch: false,
    breakpoints: {
    767: {
      slidesPerView: 1,
      pagination: '.swiper-pagination',
      paginationType: 'bullets',
      paginationClickable: true,
      }
    }
  });
});
