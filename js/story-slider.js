$(document).ready(function() {
  var galleryTop = new Swiper('.story-slider', {
    spaceBetween: 0,
    slidesPerView: 1,
    pagination: '.story-slider__pagination',
    paginationClickable: true,
    // autoplay: 3000
  });
});
