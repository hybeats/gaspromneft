var windowWidth = document.documentElement.clientWidth;

$(document).ready(function() {
  var tabsParent = $('.tabs-accordeon');
  $('.tabs-accordeon__nav-item').click(function() {
    var data = $(this).data('tab');
    tabsParent.find('.tabs-accordeon__nav-item').removeClass('active');
    tabsParent.find('[data-tab="'+ data +'"]').addClass('active');

    tabsParent.find('.tabs-accordeon__content-item').removeClass('active');
    tabsParent.find('[data-content="'+ data +'"]').addClass('active');
    var thisScroll = $(this).offset().top;
    if (windowWidth < 769) {
      $('html, body').animate({ scrollTop: thisScroll}, 500);
    }
  });

});
