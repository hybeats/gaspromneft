$(document).ready(function(){
  $('.vacancies-filter__checkbox').iCheck({
    checkboxClass: 'icheckbox_minimal',
    radioClass: 'iradio_minimal',
    increaseArea: '20%'
  });

  $('.career-vacancies__filter-btn').click(function(e) {
    e.preventDefault();
    $(this).addClass('active');
    $('.career-vacancies').find('.mobile-vac-filter').addClass('active');
  });
  $('.vacancies-filter__submit, .vacancies-filter__close').click(function(e) {
    e.preventDefault();
    $('.mobile-vac-filter').removeClass('active');
  });
});
