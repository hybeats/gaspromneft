<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<html>
  <head>
    <?$APPLICATION->ShowHead();?>
    <title><?$APPLICATION->ShowTitle()?></title>
    <?$APPLICATION->SetAdditionalCSS("/css/grid30.css");?>
    <?$APPLICATION->SetAdditionalCSS("/css/normalize.css");?>
    <?$APPLICATION->SetAdditionalCSS("/css/main.css");?>
    <?$APPLICATION->SetAdditionalCSS("/css/media.css");?>
    <?$APPLICATION->SetAdditionalCSS("/js/plugins/swiper.min.css");?>

    <?$APPLICATION->AddHeadScript('https://code.jquery.com/jquery-3.1.1.min.js');?>
    <?$APPLICATION->AddHeadScript('/js/plugins/swiper.jquery.min.js');?>
    <?$APPLICATION->AddHeadScript('/js/main.js');?>

  </head>
  <body>
  <?$APPLICATION->ShowPanel()?>
